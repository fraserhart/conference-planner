'use strict';
const chai = require('chai');
const assert = chai.assert;
const should = chai.should();
const sinon = require('sinon');

const Planner = require('../src/Planner');

describe('Planner Class', () => {

    it('should exist', () => {
        Planner.should.be.a('Function');
    });

    describe('the class ', () => {

        let planner;

        beforeEach(() => {
            planner = new Planner();
        });

        afterEach(() => {
            planner = null;
        });

        it('should have an array to store the available courses', () => {
            planner.talks.should.be.a('Array');
        });

        it('should have an array to store the proosals', () => {
            planner.proposals.should.be.a('Array');
        });

        it('should have a method to add a single talk', () => {
            planner.addSingleTalkToTalks.should.be.a('Function');
        });

        it('should have a method to add the proposals to the proposals array', () => {
            planner.getProposalsFromInput.should.be.a('Function');
        });

        it('should have a method to randomise the talks array', () => {
            planner.randomiseTalks.should.be.a('Function');
        })

        it('should have a method to extract the talk data from the proposals array', () => {
            planner.getTalkDataFromProposals.should.be.a('Function');
        });

        it('should have a method to check if the order is good', () => {
            planner.checkIfOrderWorks.should.be.a('Function');
        });

        it('should have a method to log the output from the ordered list', () => {
            planner.getFormattedOutput.should.be.a('Function');
        });

        describe('the addSingleTalkToTalks fn', () => {
            it('should take a string with a numeric number of minutes and a talk name and add it to the courses', () => {
                planner.addSingleTalkToTalks('Writing Fast Tests Against Enterprise Rails 60min');
                assert.deepEqual(planner.talks, [{name: "Writing Fast Tests Against Enterprise Rails 60min", duration: 60}]);
                planner.addSingleTalkToTalks('Rails for Python Developers lightning');
                assert.deepEqual(planner.talks, [{name: "Writing Fast Tests Against Enterprise Rails 60min", duration: 60}, {name: "Rails for Python Developers lightning", duration: 5}]);
            })
        });

        describe('the getProposalsFromInput fn', () => {
            it('should take a string with and add each line to the proposals array', () => {
                const input = "test input 1\ntest input 2";
                planner.getProposalsFromInput(input);
                assert.deepEqual(planner.proposals, ["test input 1", "test input 2"]);
            });
        });

        describe('the randomiseTalks fn', () => {
            it('should randomise the talks array', () => {
                planner.talks = [{name: "Writing Fast Tests Against Enterprise Rails 60min", duration: 60}, {name: "Rails for Python Developers lightning", duration: 5}, {name: "Another talk", duration: 7}, {name: "hello", duration: 99}];
                let prevTalks = [...planner.talks];
                planner.randomiseTalks();
                assert.notDeepEqual(prevTalks, planner.talks);
            });
        });

        describe('the getTalkDataFromProposals fn', () => {
            it('should call addSingleTalkToTalks for each element in the proposals array', () => {
                planner.proposals = ["test input 1", "test input 2"];
                const spy = sinon.spy(planner, 'addSingleTalkToTalks');
                planner.getTalkDataFromProposals();
                assert.equal(spy.calledTwice, true);

            });
        });

        describe('the checkIfOrderWorks fn', () => {
            it('should return true when the order of talks fits into the conference schedule', () => {
                planner.talks = [
                    { name: 'Writing Fast Tests Against Enterprise Rails 60min', duration: 60 },
                    { name: 'Overdoing it in Python 45min', duration: 45 },
                    { name: 'Lua for the Masses 30min', duration: 30 },
                    { name: 'Ruby Errors from Mismatched Gem Versions 45min', duration: 45 },
                    { name: 'Ruby on Rails: Why We Should Move On 60min', duration: 60 },
                    { name: 'Common Ruby Errors 45min', duration: 45 },
                    { name: 'Pair Programming vs Noise 45min', duration: 45 },
                    { name: 'Programming in the Boondocks of Seattle 30min', duration: 30 },
                    { name: 'Ruby vs. Clojure for Back-End Development 30min', duration: 30 },
                    { name: 'User Interface CSS in Rails Apps 30min', duration: 30 },
                    { name: 'Communicating Over Distance 60min', duration: 60 },
                    { name: 'Rails Magic 60min', duration: 60 },
                    { name: 'Woah 30min', duration: 30 },
                    { name: 'Sit Down and Write 30min', duration: 30 },
                    { name: 'Accounting-Driven Development 45min', duration: 45 },
                    { name: 'Clojure Ate Scala (on my project) 45min', duration: 45 },
                    { name: 'A World Without HackerNews 30min', duration: 30 },
                    { name: 'Ruby on Rails Legacy App Maintenance 60min', duration: 60 },
                    { name: 'Rails for Python Developers lightning', duration: 5 }
                ];

                assert.equal(planner.checkIfOrderWorks(), true);
            });

            it('should return false when the order of talks does not fit the conference schedule', () => {
                planner.talks = [
                    { name: 'Programming in the Boondocks of Seattle 30min', duration: 30 },
                    { name: 'Common Ruby Errors 45min', duration: 45 },
                    { name: 'A World Without HackerNews 30min', duration: 30 },
                    { name: 'Rails Magic 60min', duration: 60 },
                    { name: 'Ruby vs. Clojure for Back-End Development 30min', duration: 30 },
                    { name: 'Sit Down and Write 30min', duration: 30 },
                    { name: 'Ruby on Rails Legacy App Maintenance 60min', duration: 60 },
                    { name: 'Woah 30min', duration: 30 },
                    { name: 'Ruby on Rails: Why We Should Move On 60min', duration: 60 },
                    { name: 'Accounting-Driven Development 45min', duration: 45 },
                    { name: 'Overdoing it in Python 45min', duration: 45 },
                    { name: 'Communicating Over Distance 60min', duration: 60 },
                    { name: 'Pair Programming vs Noise 45min', duration: 45 },
                    { name: 'Rails for Python Developers lightning', duration: 5 },
                    { name: 'Writing Fast Tests Against Enterprise Rails 60min', duration: 60 },
                    { name: 'Ruby Errors from Mismatched Gem Versions 45min', duration: 45 },
                    { name: 'User Interface CSS in Rails Apps 30min', duration: 30 },
                    { name: 'Clojure Ate Scala (on my project) 45min', duration: 45 },
                    { name: 'Lua for the Masses 30min', duration: 30 }
                ];

                assert.equal(planner.checkIfOrderWorks(), false);
            });
        });

        describe('the checkIfOrderWorks fn', () => {
            it('should return true when the order of talks fits into the conference schedule', () => {
                planner.talks = [
                    {name: 'Writing Fast Tests Against Enterprise Rails 60min', duration: 60},
                    {name: 'Overdoing it in Python 45min', duration: 45},
                    {name: 'Lua for the Masses 30min', duration: 30},
                    {name: 'Ruby Errors from Mismatched Gem Versions 45min', duration: 45},
                    {name: 'Ruby on Rails: Why We Should Move On 60min', duration: 60},
                    {name: 'Common Ruby Errors 45min', duration: 45},
                    {name: 'Pair Programming vs Noise 45min', duration: 45},
                    {name: 'Programming in the Boondocks of Seattle 30min', duration: 30},
                    {name: 'Ruby vs. Clojure for Back-End Development 30min', duration: 30},
                    {name: 'User Interface CSS in Rails Apps 30min', duration: 30},
                    {name: 'Communicating Over Distance 60min', duration: 60},
                    {name: 'Rails Magic 60min', duration: 60},
                    {name: 'Woah 30min', duration: 30},
                    {name: 'Sit Down and Write 30min', duration: 30},
                    {name: 'Accounting-Driven Development 45min', duration: 45},
                    {name: 'Clojure Ate Scala (on my project) 45min', duration: 45},
                    {name: 'A World Without HackerNews 30min', duration: 30},
                    {name: 'Ruby on Rails Legacy App Maintenance 60min', duration: 60},
                    {name: 'Rails for Python Developers lightning', duration: 5}
                ];

                const expectedOutput = `Track 1:\n09:00AM Writing Fast Tests Against Enterprise Rails 60min\n10:00AM Overdoing it in Python 45min\n10:45AM Lua for the Masses 30min\n11:15AM Ruby Errors from Mismatched Gem Versions 45min\n12:00PM Lunch\n01:00PM Ruby on Rails: Why We Should Move On 60min\n02:00PM Common Ruby Errors 45min\n02:45PM Pair Programming vs Noise 45min\n03:30PM Programming in the Boondocks of Seattle 30min\n04:00PM Ruby vs. Clojure for Back-End Development 30min\n04:30PM User Interface CSS in Rails Apps 30min\n05:00PM Networking Event\n\nTrack 2:\n09:00AM Communicating Over Distance 60min\n10:00AM Rails Magic 60min\n11:00AM Woah 30min\n11:30AM Sit Down and Write 30min\n12:00PM Lunch\n01:00PM Accounting-Driven Development 45min\n01:45PM Clojure Ate Scala (on my project) 45min\n02:30PM A World Without HackerNews 30min\n03:00PM Ruby on Rails Legacy App Maintenance 60min\n04:00PM Rails for Python Developers lightning\n05:00PM Networking Event\n`;

                assert.equal(planner.getFormattedOutput(), expectedOutput);
            });
        });
    });

});