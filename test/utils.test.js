'use strict';
const chai = require('chai');
const assert = chai.assert;
const should = chai.should();
const sinon = require('sinon');

const Utils = require('../src/Utils');

describe('Utils', () => {

    describe('padNumber function', () => {
        it('should correctly pad a number with a leading zero', () => {
            assert.deepEqual(Utils.padNumber(5), "05");
        });
    });
});