module.exports = {
    padNumber: (number) => {
        return ("0" + number).slice(-2);
    }
}