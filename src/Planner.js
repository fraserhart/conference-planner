const Utils = require('./Utils');

class Planner {
    constructor() {
        this.talks = [];
        this.proposals = [];
        this.availableSessions = [
            {track: 1,
                sessions:
                [
                    {startTime: 9, min: 0, max: 180, afterSessionActivity: {name: 'Lunch', startTime: 12}},
                    {startTime: 13, min: 180, max: 240, afterSessionActivity: {name: 'Networking Event', startTime: 17}}
                ]
            },
            {track: 2,
                sessions:
                [
                    {startTime: 9, min: 0, max: 180, afterSessionActivity: {name: 'Lunch', startTime: 12}},
                    {startTime: 13, min: 180, max: 240, afterSessionActivity: {name: 'Networking Event', startTime: 17}}
                ]
            }
        ];
    }

    getProposalsFromInput( input ){
        this.proposals = input.split(/\r\n|\r|\n/g);
    }

    getTalkLengthFromTitle( talkString ){
        const regex = /(\d+)/g;
        return talkString.indexOf('lightning') > -1? 5 : Number(talkString.match(regex)[0]);
    }

    addSingleTalkToTalks( talkString ){
        let talkLengthInMinutes = this.getTalkLengthFromTitle( talkString );
        this.talks.push({name: talkString, duration: talkLengthInMinutes });
    }

    getTalkDataFromProposals(){
        this.proposals.forEach((proposal) => {
            this.addSingleTalkToTalks(proposal);
        }, this);
    }

    randomiseTalks(){
        let currentIndex = this.talks.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = this.talks[currentIndex];
            this.talks[currentIndex] = this.talks[randomIndex];
            this.talks[randomIndex] = temporaryValue;
        }
    }

    getFlattenedArrayOfSessions(){
        let availableSessions = [];
        this.availableSessions.forEach((track)=>{
            track.sessions.forEach(session => {
                availableSessions.push(JSON.parse(JSON.stringify(session)));
            });
        });
        return availableSessions;
    }

    checkIfOrderWorks(){
        let availableSessions = this.getFlattenedArrayOfSessions();
        let plannedSessions = [];
        let orderOfSessionsFitsPlanner = true;
        let total = 0;

        for (let talk of this.talks){
            total += talk.duration;
            if (total > availableSessions[0].max) {
                plannedSessions.push(availableSessions.shift());
                if (availableSessions.duration < 1){
                    orderOfSessionsFitsPlanner = false;
                    break;
                }
                total = talk.duration;
            } else if (total - talk.duration < availableSessions[0].min && total > availableSessions[0].max){
                orderOfSessionsFitsPlanner = false;
                break;
            }
        }

        if (!orderOfSessionsFitsPlanner){
            return false;
        } else if (availableSessions.length < 1){
            return false;
        } else if (total < availableSessions[0].min){
            return false;
        } else {
            return true;
        }
    }

    addActivityLine(dateObj, activityName){
        let amPM = dateObj.getHours() > 11 ? "PM" : "AM";
        let activityHours = dateObj.getHours() > 12 ? dateObj.getHours() - 12 : dateObj.getHours();
        return `${Utils.padNumber(activityHours)}:${Utils.padNumber(dateObj.getMinutes())}${amPM} ${activityName}\n`;
    }

    getFormattedOutput(){
        let formattedOutput = "";
        let dateObj = new Date();
        let allTalksQueue = JSON.parse(JSON.stringify(this.talks));
        this.availableSessions.forEach((track, i) => {
            formattedOutput += `Track ${track.track}:\n`;
            track.sessions.forEach(session => {
                let addedSessionLength = 0;
                dateObj.setHours(session.startTime, 0, 0, 0);
                while (allTalksQueue.length && (addedSessionLength + allTalksQueue[0].duration <= session.max)){
                    const nextTalk = allTalksQueue.shift();
                    formattedOutput += this.addActivityLine(dateObj, nextTalk.name);
                    addedSessionLength += nextTalk.duration;
                    dateObj.setMinutes(dateObj.getMinutes() + nextTalk.duration);
                }
                dateObj.setHours(session.afterSessionActivity.startTime,0,0,0);
                formattedOutput += this.addActivityLine(dateObj, session.afterSessionActivity.name);
            });
            if (i < this.availableSessions.length-1){
                formattedOutput += `\n`;
            }

        }, this);
        return formattedOutput;
    }

    getConferencePlan(proposals){
        this.getProposalsFromInput(proposals);
        this.getTalkDataFromProposals();
        while (!this.checkIfOrderWorks()){
            this.randomiseTalks();
        }
        return this.getFormattedOutput();

    }


}

module.exports = Planner;