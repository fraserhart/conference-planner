const fs = require('fs');
const Planner = require('./src/Planner');
let planner;

const readFiles = (dirname, onFileContent, onError) => {
    fs.readdir(dirname, function(err, filenames) {
        if (err) {
            onError(err);
            return;
        }
        filenames.forEach(function(filename) {
            fs.readFile(dirname + filename, 'utf-8', function(err, content) {
                if (err) {
                    onError(err);
                    return;
                }
                onFileContent(content);
            });
        });
    });
};



readFiles('./data/', content => {
    planner = new Planner();
    console.log(planner.getConferencePlan(content));
}, err => {
    console.log(err);
});